$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document.body).on('click', '.form-delete', function (e) {
        e.preventDefault();
        let $a = $(this);
        let message = $a.data('confirm') ? $a.data('confirm') : 'Are you sure want to do this ?';
        mConfirm(message);
        $("#yes-modal-confirm").on('click', function () {
            let url = $a.data('url');
            let type = 'delete';
            postData(url, type, data = {}, function (err, response) {
                if (response) {
                    if ($.isEmptyObject(response.error)) {
                        baris = $('#form-' + response.id).closest('tr');
                        baris.fadeOut(200, function () {
                            $a.remove();
                        });
                    } else {
                        mError(response.error);
                    }
                } else {
                    console.log('ini error : ', err);
                }
            });
        });
    });

    $(document.body).on('submit', '.form-save', function (e) {
        e.preventDefault();
        let $form = $(this);
        let data = $form.serialize();
        let type = $form.attr('method');
        let url = $form.attr('action');
        let rule = $form.data('rule');
        let btn = $form.data('btn');
        $(".form-group").removeClass("has-error");
        $("#"+btn).val("Loading..");
        postData(url, type, data, function (err, response) {
            if (response) {
                if ($.isEmptyObject(response.error)) {
                    saveResponse($form, rule);
                    mSuccess(response.success);
                    $("#"+btn).val("Simpan");
                } else {
                    mErrorArray(response.error);
                    $("#"+btn).val("Simpan");
                }

            } else {
                console.log('ini error : ', err);
            }
        });
    });

});


function saveResponse($form, rule){
    if (rule == "store") {
        $form.trigger("reset");

    } else if (rule == "store-response") {
        $form.trigger("reset");
        fillSomething($form.data('response'), $form.data('target'));

    } else if (rule == "save-harga") {
        $("#last-record").html("<tr><td>-</td><td>-</td><td>-</td><td>-</td></tr>");
        setTimeout(() => {
            $form.trigger("reset");
        }, 250);

    } else {
        setTimeout(() => {
            window.location = $form.data('redirect');
        }, 250);

    }
}

function postData(url, type, data, callback) {
    $.ajax({
        url: url,
        type: type,
        dataType: "json",
        data: data,
        success: function (response) {
            return callback(null, response);
        },
        error: function (err) {
            return callback(true, err);
        }
    });
}

let modalConfirm = $('#btn-c');
let modalError = $('#btn-e');
let modalErrorArray = $('#btn-e-array');
let modalSuccess = $('#btn-s');

function mConfirm(message) {
    modalConfirm.click();
    $("#message-btn-c").html(message);
}

function mError(message) {
    modalError.click();
    $("#message-btn-e").html(message);
}

function mErrorArray(message) {
    modalErrorArray.click();
    $("#message-btn-e-array").find("ul").html('');
    $("#message-btn-e-array").css('display', 'block');
    $.each(message, function (key, value) {
        $(".group-" + key).addClass("has-error");
        $.each(value, function (k, val) {
            $("#message-btn-e-array").find("ul").append('<li>' + val + '</li>');
        });
    });
}

function mSuccess(message) {
    modalSuccess.click();
    $("#message-btn-s").html(message);
}

function resetModal() {
    $("#message-btn-c").html('');
    $("#message-btn-e").html('');
    $("#message-btn-s").html('');
}

function loadView(url, div) {
    $.ajax({
        url: url,
        success: function (data) {
            $(div).html(data);
        }
    });
}

function fillSomething(url, form) {
    $.ajax({
        url: url,
        success: function (data) {
            $(form).val(data);
        }
    });
}

function futsalTimeJS(rentTime)
{
    if(rentTime <=9){
        return rentTime = "0"+rentTime+".00";
    }else{
        return rentTime = rentTime+".00";
    }
}