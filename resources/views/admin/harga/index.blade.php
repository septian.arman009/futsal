@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Harga</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Harga</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('harga.create') }}">Tambah Harga</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="hargas" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Lapangan</th>
                                        <th>Hari</th>
                                        <th>Waktu Awal</th>
                                        <th>Waktu Akhir</th>
                                        <th>Harga</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('lapangan', null, ['class' => 'form-control', 'placeholder' => 'Lapangan', 'id' => 'lapangan']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('hari', null, ['class' => 'form-control', 'placeholder' => 'Hari', 'id' => 'hari']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('waktu_awal', null, ['class' => 'form-control', 'placeholder' => 'Waktu Awal', 'id' => 'waktu_awal']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('waktu_akhir', null, ['class' => 'form-control', 'placeholder' => 'Waktu Akhir', 'id' => 'waktu_akhir']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('harga', null, ['class' => 'form-control', 'placeholder' => 'Harga', 'id' => 'harga']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#hargas').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ route('harga.index') }}",
                data: function (d) {
                    d.lapangan = $('#lapangan').val(),
                    d.hari = $('#hari').val(),
                    d.waktu_awal = $('#waktu_awal').val(),
                    d.waktu_akhir = $('#waktu_akhir').val(),
                    d.harga = $('#harga').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'lapangan',
                    name: 'lapangan'
                },
                {
                    data: 'hari',
                    name: 'hari'
                },
                {
                    data: 'waktu_awal',
                    name: 'waktu_awal'
                },
                {
                    data: 'waktu_akhir',
                    name: 'waktu_akhir'
                },
                {
                    data: 'harga',
                    name: 'harga'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#lapangan').on('change', function () {
            table.draw();
        });
        $('#hari').on('change', function () {
            table.draw();
        });
        $('#waktu_awal').on('change', function () {
            table.draw();
        });
        $('#waktu_akhir').on('change', function () {
            table.draw();
        });
        $('#harga').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #hargas_filter {
        display: none;
    }
    #hargas_length {
        width: 100%;
    }
</style>
@endsection