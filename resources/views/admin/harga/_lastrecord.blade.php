@if ($last_record->count() > 0)
@foreach ($last_record as $harga)
    <tr>
        <td>{{ $harga->hari }}</td>
        <td>{{ futsalTime($harga->waktu_awal) }}</td>
        <td>{{ futsalTime($harga->waktu_akhir) }}</td>
        <td>{{ torp($harga->harga) }}</td>
    </tr>

@endforeach
@else
    <tr>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
@endif