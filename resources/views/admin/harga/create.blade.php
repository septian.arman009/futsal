@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li>Harga</li>
    <li class="active">Tambah Data</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Harga</h3>
                </div>

                {!! Form::open(['url' => route('harga.store'),
                    'class' => 'form-horizontal form-save',
                    'method' =>'post',
                    'data-btn' => 'btn-save',
                    'data-rule' => 'save-harga']) !!}
                    
                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('harga.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.harga._form')
                        </div>
                        <div class="col-md-6">
                            <div>
                                <div class="panel panel-default">
                                   <table class="table table-bordered table-hover">
                                       <thead>
                                           <th>Hari</th>
                                           <th>Waktu Awal</th>
                                           <th>Waktu Akhir</th>
                                           <th>Harga</th>
                                       </thead>
                                       <tbody id="last-record">
                                           <tr>
                                              <td>-</td>
                                              <td>-</td>
                                              <td>-</td>
                                              <td>-</td>
                                           </tr>
                                       </tbody>
                                   </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection