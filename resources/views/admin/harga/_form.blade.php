<div class="form-group group-lapangan_id">
    {!! Form::label('lapangan_id', 'Lapangan', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-flag"></span>
            </span>
            {!! Form::select('lapangan_id', ['' => '- Pilih Lapangan -']+App\Lapangan::pluck('nama', 'id')->all(), 
            null, ['class' => 'form-control', 'id' => 'lapangan_id', 'onchange' => 'last_record()']) !!}
        </div>
    </div>
</div>

<div class="form-group group-hari">
    {!! Form::label('nama', 'Hari', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
            </span>
            {!! Form::select('hari', $hari, null, ['class' => 'form-control', 'id' => 'hari', 'onchange' => 'last_record()']) !!}
        </div>
    </div>
</div>

<div class="form-group group-waktu_awal">
    {!! Form::label('waktu_awal', 'Waktu Awal', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
            </span>
            {!! Form::select('waktu_awal', waktu(), null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group group-waktu_akhir">
    {!! Form::label('waktu_akhir', 'Waktu Akhir', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
            </span>
            {!! Form::select('waktu_akhir', waktu(), null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group group-harga">
    {!! Form::label('harga', 'Harga', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-money"></span>
            </span>
            {!! Form::number('harga', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function(){
            last_record();
        });

        function last_record(){
            let lapangan_id = $("#lapangan_id").val();
            let hari = $("#hari").val();

            if(lapangan_id != '' && hari != ''){
                loadView("{{ url('admin/last_record_harga') }}/"+lapangan_id+"/"+hari, '#last-record');
            }
        }
    </script>
@endsection