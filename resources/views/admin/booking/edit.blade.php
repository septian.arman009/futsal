@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li>Daftar Booking</li>
    <li class="active">Edit Jam Booking</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Data Booking</h3>
                </div>
                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('booking.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">                        
                                <a href="#" class="tile tile-default">
                                    {{$hari}}
                                    <p>{{$indoFullDate}}</p>
                                    <div class="informer informer-primary"></div>
                                </a>                        
                            </div>
                        @php $form = 1; @endphp
                        @foreach ($lapangan as $_lapangan)
                        <div class="row">
                            <div class="col-md-4">
                                <a class="tile tile-default" style="border-top: 1px solid black;">
                                    {{$_lapangan->nama}}
                                    <p>Waktu Sewa <b>{{ $member }}</b> adalah <b>{{$waktu_member}}</b></p>
                                </a>
                            </div>
                            <div class="col-md-8" style="border-top: 1px solid black;">
                                @if ($_lapangan->harga->count() > 0)
                                    @if ($min > 0)
                                        @for ($i = $min; $i <= $max-1; $i++)
                                        {!! Form::open(['url' => route('booking.form_booking_edit'), 'method' => 'post', 'id' => 'form-'.$form]) !!}
                                        <div class="panel panel-default" style="width: 32%; margin-right: 1%; {{ ($loop->iteration <= 3) ? "margin-top: 5px;" : "" }}">
                                            <div class="panel-body list-group list-group-contacts">
                                                @if (in_array($i, $jam))
                                                    @if (in_array($i, $jamMember))
                                                        <a class="list-group-item" onclick="formBooking('#form-{{$form}}')" style="background-color: #865050; color: #95b75d;">
                                                    @else
                                                        <a class="list-group-item" style="background-color: #ccc; color: #95b75d;">
                                                    @endif
                                                @else
                                                <a class="list-group-item" onclick="formBooking('#form-{{$form}}')">
                                                @endif
                                                    <div class="list-group-status status-online"></div>
                                                    <span class="contacts-title" id="jam">{{ futsalTime($i) }}</span>
                                                    <p id="harga">
                                                        @php 
                                                        
                                                        $harga = App\Harga::where([
                                                            ['lapangan_id', $_lapangan->id],
                                                            ['hari', $hari],
                                                            ['waktu_awal', '<=', $i],
                                                            ['waktu_akhir', '>=', $i]
                                                        ]);
                                                        
                                                        if($harga->count() > 0){
                                                            echo torp($harga->first()->harga);
                                                        }else{
                                                            echo "Tidak ada jadwal di jam ini";
                                                        }
                                                        $form++;
                                                        @endphp
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                        {!! Form::number('booking_id', $booking_id, ['style' => 'display:none']) !!}
                                        {!! Form::number('jadwal_id', $jadwal_id, ['style' => 'display:none']) !!}
                                        {!! Form::number('waktu_awal', $i, ['style' => 'display:none']) !!}
                                        {!! Form::number('lapangan_id', $_lapangan->id, ['style' => 'display:none']) !!}
                                        {!! Form::text('hari', $hari, ['style' => 'display:none']) !!}
                                        {!! Form::text('date', $date, ['style' => 'display:none']) !!}
                                        {!! Form::close() !!}
                                        @endfor
                                    @endif
                                @endif
                            </div>
                        @endforeach
                       </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('style')
<style>
    #jam {
        font-size: 20px;
    }
    #harga {
        font-size: 18px;
    }
    a{
        cursor: pointer;
    }
</style>
@endsection

@section('scripts')
    <script>
        function formBooking(form){
            $(form).submit();
        }
    </script>
@endsection