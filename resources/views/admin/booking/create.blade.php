@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li class="active">Booking Lapangan</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Jam Operasional</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group group-tanggal">
                                <label for="tahun">Ganti Tanggal</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <input type="text" class="form-control" id="datepicker" value="{{ $date }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>                      
                            <a href="#" class="tile tile-default">
                                {{$hari}}
                                <p>{{$indoFullDate}}</p>
                                <div class="informer informer-primary"></div>
                            </a>                        
                        </div>
                        @php $form = 1; @endphp
                        @foreach ($lapangan as $_lapangan)
                        <div class="row">
                            <div class="col-md-4">
                                <a class="tile tile-default" style="border-top: 1px solid black;">
                                    {{$_lapangan->nama}}
                                    <p>Jumlah Booking : <b>{{$memberBooked[$loop->iteration-1]}}</b></p>
                                </a>
                            </div>
                            <div class="col-md-8" style="border-top: 1px solid black;">
                                @if ($_lapangan->harga->count() > 0)
                                    @if ($min[$loop->iteration-1] > 0)
                                        @for ($i = $min[$loop->iteration-1] ; $i <= $max[$loop->iteration-1]-1; $i++)
                                        {!! Form::open(['url' => route('booking.form_booking'), 'method' => 'post', 'id' => 'form-'.$form]) !!}
                                        <div class="panel panel-default" style="width: 32%; margin-right: 1%; {{ ($loop->iteration <= 3) ? "margin-top: 5px;" : "" }}">
                                            <div class="panel-body list-group list-group-contacts">
                                                @php
                                                    $harga = App\Harga::where([
                                                        ['lapangan_id', $_lapangan->id],
                                                        ['hari', $hari],
                                                        ['waktu_awal', '<=', $i],
                                                        ['waktu_akhir', '>=', $i]
                                                    ]); 
                                                @endphp

                                                @if (in_array($i, $jamAktif[$loop->iteration-1]))
                                                    <a class="list-group-item" style="background-color: #ccc;color: #95b75d;">
                                                @else
                                                    @if($harga->count() > 0)
                                                        <a class="list-group-item" onclick="formBooking('#form-{{$form}}')">
                                                    @else
                                                        <a class="list-group-item">
                                                    @endif
                                                @endif
                                                    <div class="list-group-status status-online"></div>
                                                    <span class="contacts-title" id="jam">{{ futsalTime($i) }}</span>
                                                    <p id="harga">
                                                        @php 

                                                        if($harga->count() > 0){
                                                            echo torp($harga->first()->harga);
                                                        }else{
                                                            echo "Tidak ada jadwal";
                                                        }
                                                        $form++;

                                                        @endphp
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                        {!! Form::number('waktu_awal', $i, ['style' => 'display:none']) !!}
                                        {!! Form::number('lapangan_id', $_lapangan->id, ['style' => 'display:none']) !!}
                                        {!! Form::text('hari', $hari, ['style' => 'display:none']) !!}
                                        {!! Form::text('date', $date, ['style' => 'display:none']) !!}
                                        {!! Form::close() !!}
                                        @endfor
                                    @endif
                                @endif
                            </div>
                        @endforeach
                       </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('style')
<style>
    #jam {
        font-size: 20px;
    }
    #harga {
        font-size: 18px;
    }
    a{
        cursor: pointer;
    }
</style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker({format: 'yyyy-mm-dd'}); 
        });

        function formBooking(form){
            $(form).submit();
        }

        $("#datepicker").on("change", function(){
            window.location.href = "{{ url('admin/transaksi/booking/create') }}/"+$(this).val();
        });
    </script>
@endsection