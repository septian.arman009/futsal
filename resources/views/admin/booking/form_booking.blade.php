@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li>Booking Lapangan</li>
    <li class="active">Tambah Data</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Booking</h3>
                </div>

                {!! Form::open(['url' => route('booking.store'),
                'class' => 'form-horizontal form-save',
                'method' =>'post',
                'data-btn' => 'btn-save',
                'data-redirect' => route('booking.create', $date)]) !!}

                {!! Form::number('waktu_awal', $waktu_awal, ['style' => 'display:none', 'id' => 'waktu_awal']) !!}
                {!! Form::number('lapangan_id', $lapangan_id, ['style' => 'display:none']) !!}
                {!! Form::text('hari', $hari, ['style' => 'display:none']) !!}
                {!! Form::text('date', $date, ['style' => 'display:none']) !!}

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('booking.create', $date) }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <a class="tile tile-default">
                                <div id="waktu">{{futsalTime($waktu_awal)}}</div>
                                {!! Form::select('user_id', $members, null, ['class' => 'form-control']) !!}
                                <hr>
                                {!! Form::select('durasi', $jam, null, ['class' => 'form-control', 'id' => 'durasi']) !!}
                            </a>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <div class="panel panel-default">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <th>Lapangan</th>
                                            <th>Jam Masuk</th>
                                            <th>Jam Keluar</th>
                                            <th>Member</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($details as $detail)
                                            <tr>
                                                <td>{{$detail->lapangan->nama}}</td>
                                                <td>{{futsalTime($detail->waktu_awal)}}</td>
                                                <td>{{futsalTime($detail->waktu_akhir)}}</td>
                                                <td>{{$detail->booking->user->name}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $("#durasi").on("change", function(){
            let waktu_awal = $("#waktu_awal").val();
            let durasi = $(this).val();
            let waktu_akhir = parseInt(waktu_awal)+parseInt(durasi);
            $("#waktu").html(futsalTimeJS(waktu_awal)+' - '+futsalTimeJS(waktu_akhir));
        });
    </script>
@endsection