@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li class="active">Daftar Booking</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Booking</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="bookings" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Member</th>
                                        <th>Lapangan</th>
                                        <th width="16%">Tanggal</th>
                                        <th width="12%">Jam Masuk</th>
                                        <th width="12%">Jam Keluar</th>
                                        <th>Tagihan</th>
                                        <th>Status</th>
                                        <th width="16%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('member', null, ['class' => 'form-control', 'placeholder' => 'Member', 'id' => 'member']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('lapangan', null, ['class' => 'form-control', 'placeholder' => 'Lapangan', 'id' => 'lapangan']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'Tanggal', 'id' => 'date']) !!}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            {!! Form::text('tagihan', null, ['class' => 'form-control', 'placeholder' => 'Tagihan', 'id' => 'tagihan']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('status', null, ['class' => 'form-control', 'placeholder' => 'Status', 'id' => 'status']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#bookings').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ route('booking.index') }}",
                data: function (d) {
                    d.member = $('#member').val(),
                    d.lapangan = $('#lapangan').val(),
                    d.date = $('#date').val(),
                    d.waktu_awal = $('#waktu_awal').val(),
                    d.waktu_akhir = $('#waktu_akhir').val(),
                    d.tagihan = $('#tagihan').val(),
                    d.status = $('#status').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'member',
                    name: 'member'
                },
                {
                    data: 'lapangan',
                    name: 'lapangan'
                },
                {
                    data: 'date',
                    name: 'date'
                },
                {
                    data: 'waktu_awal',
                    name: 'waktu_awal'
                },
                {
                    data: 'waktu_akhir',
                    name: 'waktu_akhir'
                },
                {
                    data: 'tagihan',
                    name: 'tagihan'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#member').on('change', function () {
            table.draw();
        });
        $('#lapangan').on('change', function () {
            table.draw();
        });
        $('#date').on('change', function () {
            table.draw();
        });
        $('#tagihan').on('change', function () {
            table.draw();
        });
        $('#status').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #bookings_filter {
        display: none;
    }
    #bookings_length {
        width: 100%;
    }
</style>
@endsection