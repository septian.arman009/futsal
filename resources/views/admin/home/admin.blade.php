@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li class="active">Beranda</li>
</ul>

<div class="page-content-wrap">
    <div class="col-md-12">
        @include('admin.layouts._flash')
    </div>
    <div class="row row-bg">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dashboard</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <a class="tile tile-default">
                            <h2>{{ $total_pendapatan }}</h2>
                            <p>Total Pendapatan Bulan <b>{{ $bulan }} {{ $tahun }}</b></p>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <div id="pendapatan" style="height: 300px;"></div>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <h2>Lapangan Terlaris</h2>
                        <div id="donut" style="height: 300px;"></div>
                    </div>
                    <div class="col-md-8">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Lapangan</th>
                                    <th>Jumlah Sewa</th>
                                    <th>Total Pendapatan</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($lapangan as $_lp)
                                    <tr>
                                        <td>{{ $_lp['nama'] }}</td>
                                        <td>{{ $_lp['durasi'] }} Jam</td>
                                        <td>{{ torp($_lp['total_pendapatan']) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection


@section('scripts')
<script>
    Morris.Bar({
        element: 'pendapatan',
        data: {!!json_encode($bar) !!},
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Pendapatan'],
        barRatio: 0.4,
        barColors: ['#5c7572'],
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'donut',
        data: {!! json_encode($donut) !!},
        colors: {!! json_encode($color) !!}
    });
</script>
@endsection
