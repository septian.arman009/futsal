@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li class="active">Beranda</li>
</ul>

<div class="page-content-wrap">
    <div class="col-md-12">
        @include('admin.layouts._flash')
    </div>
    <div class="row row-bg">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dashboard</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <table id="members" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th>Nama Member</th>
                                    <th>Tanggal</th>
                                    <th>Lapangan</th>
                                    <th>Jam Masuk</th>
                                    <th>Jam Keluar</th>
                                    <th>Tagihan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection


@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#members').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ url('admin/home') }}"
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'member',
                    name: 'member'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'lapangan',
                    name: 'lapangan'
                },
                {
                    data: 'jam_masuk',
                    name: 'jam_masuk'
                },
                {
                    data: 'jam_keluar',
                    name: 'jam_keluar'
                },
                {
                    data: 'tagihan',
                    name: 'tagihan'
                },
                {
                    data: 'status',
                    name: 'status'
                }
            ]
        });
    });
</script>
@endsection