@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li class="active">Ganti Password</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Password</h3>
                </div>
                {!! Form::open(['url' => route('store_password'),
                'class' => 'form-horizontal form-save',
                'method' =>'post',
                'data-btn' => 'btn-save',
                'data-rule' => 'store']) !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group group-current_password">
                                {!! Form::label('current_password', 'Password Lama', ['class' => 'col-md-3 control-label'])
                                !!}
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="fa fa-lock"></span>
                                        </span>
                                        {!! Form::password('current_password', ['class' => 'form-control']) !!}
                                    </div>
                                    {!! $errors->first('current_password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group group-new_password">
                                {!! Form::label('new_password', 'Password Baru', ['class' => 'col-md-3 control-label'])
                                !!}
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="fa fa-lock"></span>
                                        </span>
                                        {!! Form::password('new_password', ['class' => 'form-control']) !!}
                                    </div>
                                    {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group group-new_confirm_password">
                                    {!! Form::label('new_confirm_password', 'Konfirmasi Password Baru', ['class' => 'col-md-3 control-label'])
                                    !!}
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="fa fa-lock"></span>
                                            </span>
                                            {!! Form::password('new_confirm_password', ['class' => 'form-control']) !!}
                                        </div>
                                        {!! $errors->first('new_confirm_password', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection