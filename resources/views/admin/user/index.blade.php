@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Pengguna</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Pengguna</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('users.create') }}">Tambah Pengguna</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="users" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama', 'id' => 'name']) !!}
                                        </td>
                                        <td>
                                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'id' => 'email']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('role', null, ['class' => 'form-control', 'placeholder' => 'Role', 'id' => 'role']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#users').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ route('users.index') }}",
                data: function (d) {
                    d.name = $('#name').val(),
                    d.email = $('#email').val(),
                    d.role = $('#role').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'role',
                    name: 'role'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#name').on('change', function () {
            table.draw();
        });

        $('#email').on('change', function () {
            table.draw();
        });

        $('#role').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #users_filter {
        display: none;
    }
    #users_length {
        width: 100%;
    }
</style>
@endsection