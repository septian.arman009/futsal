@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li class="active">Daftar Tagihan Member</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Tagihan Member</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="users" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nama</th>
                                        <th>Tagihan Aktif</th>
                                        <th>Tagihan Pending</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama', 'id' => 'name']) !!}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#users').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ route('tagihan.index') }}",
                data: function (d) {
                    d.name = $('#name').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'tagihan',
                    name: 'tagihan'
                },
                {
                    data: 'pending',
                    name: 'pending'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#name').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #users_filter {
        display: none;
    }
    #users_length {
        width: 100%;
    }
</style>
@endsection