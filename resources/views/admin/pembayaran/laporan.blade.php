@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Pembayaran</li>
    <li class="active">Laporan Pembayaran</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Pembayaran Member</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <a class="tile tile-default">
                                <h2>{{ $total_pendapatan }}</h2>
                                <p>Total Pendapatan Tahun {{ $tahun }}</p>
                            </a>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <div class="form-group group-tahun">
                                <label for="tahun">Ganti Tahun</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <select id="tahun" class="form-control">
                                        @for ($i = 2019; $i <= 2099; $i++) <option
                                            {{ ($i == $tahun) ? "selected" : "" }} value="{{ $i }}">
                                            {{ $i }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="pendapatan" style="height: 300px;"></div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <table id="users" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nama</th>
                                        <th>Pembayaran Masuk</th>
                                        <th>Pending</th>
                                        <th>Tanggal Pembayaran</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' =>
                                            'Nama', 'id' => 'name']) !!}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            {!! Form::number('tanggal', null, ['class' => 'form-control', 'placeholder'
                                            =>
                                            'Tanggal', 'id' => 'tanggal']) !!}
                                            <br>
                                            {!! Form::select('date', [
                                            '' => 'Pilih Bulan',
                                            'Januari' => 'Januari',
                                            'Februari' => 'Februari',
                                            'Maret' => 'Maret',
                                            'April' => 'April',
                                            'Mei' => 'Mei',
                                            'Juni' => 'Juni',
                                            'Juli' => 'Juli',
                                            'Agustus' => 'Agustus',
                                            'September' => 'Septemmber',
                                            'Oktober' => 'Oktober',
                                            'November' => 'November',
                                            'Desember' => 'Desember'
                                            ],null, ['class' => 'form-control', 'id' => 'date']) !!}

                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#users').DataTable({
            processing: false,
            serverSide: true,
            order: [
                [0, 'desc']
            ],
            ajax: {
                url: "{{ url('admin/pembayaran/laporan') }}/"+{{$tahun}},
                data: function (d) {
                    d.name = $('#name').val(),
                        d.date = $("#date").val(),
                        d.tanggal = $("#tanggal").val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'tagihan',
                    name: 'tagihan'
                },
                {
                    data: 'pending',
                    name: 'pending'
                },
                {
                    data: 'date',
                    name: 'date'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#name').on('change', function () {
            table.draw();
        });

        $('#date').on('change', function () {
            table.draw();
        });

        $('#tanggal').on('change', function () {
            table.draw();
        });

    });

    Morris.Bar({
        element: 'pendapatan',
        data: {!!json_encode($bar) !!},
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Pendapatan'],
        barRatio: 0.4,
        barColors: ['#5c7572'],
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });


    $("#tahun").on("change", function () {
        window.location.href = "{{ url('admin/pembayaran/laporan') }}/"+$(this).val();
    });
</script>
@endsection

@section('style')
<style>
    #users_filter {
        display: none;
    }

    #users_length {
        width: 100%;
    }
</style>
@endsection