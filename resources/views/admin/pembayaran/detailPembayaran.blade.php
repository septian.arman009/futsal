@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Pembayaran</li>
    <li>Laporan Pembayaran</li>
    <li class="active">Detail</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Pembayaran Member</h3>
                </div>
                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('pembayaran.laporan', $tahun) }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    @include('admin.pembayaran._detailForm')
                </div>

            </div>
        </div>
    </div>
</div>
@endsection