
<div class="row">
    <div class="col-md-12">                        
        <a href="#" class="tile tile-default">
            {{ $member->name }}
            <p style="color:green" id="status">Member sejak : <b>{{ indoFullDateDay($member->created_at) }}</b></p>
            <div class="informer informer-primary"></div>
        </a>                        
    </div>
    <div class="row">
        <div class="col-md-4">
            <a class="tile tile-default">
                {{ $tagihan }}
                <p>Total Tagihan Dari <b>{{ $total_booking }}</b>x Penyewaan Lapangan</p>
                <p></p>
            </a>
            @if($title != 'Detail Tagihan Member')
            <a class="tile tile-default">
                {{ $total_bayar }}
                <p>Pembayaran Masuk</p>
                <p></p>
            </a>
            @endif
        </div>
        <div class="col-md-8">
           <table class="table table-bordered table-hover">
               <thead>
                   <tr>
                       <th>Lapangan</th>
                       <th>Jam Masuk</th>
                       <th>Jam Keluar</th>
                       <th>Tanggal</th>
                       <th>Tagihan</th>
                       <th>Action</th>
                   </tr>
               </thead>

               <tbody>
                   @foreach ($bookings as $booking)
                    <tr>
                        <td>{{ $booking->jadwal->lapangan->nama }}</td>
                        <td>{{ futsalTime($booking->jadwal->waktu_awal) }}</td>
                        <td>{{ futsalTime($booking->jadwal->waktu_akhir) }}</td>
                        <td>{{ $booking->jadwal->hari.', '.indoFullDateDay($booking->jadwal->created_at) }}</td>
                        <td>{{ torp($booking->tagihan) }}</td>
                        <td>
                            @if ($booking->status == 'Lunas')
                                {!! Form::open(['url' => route('pembayaran.koreksi'),
                                    'class' => 'form-horizontal form-save',
                                    'method' =>'post',
                                    'data-btn' => 'btn-save-'.$loop->iteration,
                                    'data-redirect' => request()->url() ]) !!}
                                {!! Form::number('booking_id', $booking->id, ['style' => 'display:none']) !!}
                                {!! Form::submit('Koreksi', ['class'=>'btn btn-default pull-right', 'id' => 'btn-save-'.$loop->iteration]) !!}
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['url' => route('tagihan.store'),
                                    'class' => 'form-horizontal form-save',
                                    'method' =>'post',
                                    'data-btn' => 'btn-save-'.$loop->iteration,
                                    'data-redirect' => request()->url() ]) !!}
                                {!! Form::number('booking_id', $booking->id, ['style' => 'display:none']) !!}
                                {!! Form::submit('Bayar...', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save-'.$loop->iteration]) !!}
                                {!! Form::close() !!}
                            @endif
                            
                        </td>
                    </tr>
                   @endforeach
                    <tr>
                        <td colspan=4 class="text-right">Sisa Tagihan</td>
                        @if($title == 'Detail Tagihan Member')
                            <td><b>{{ $tagihan }}</b></td>
                        @else
                            <td><b>{{ $sisa }}</b></td>
                        @endif
                        
                        <td></td>
                    </tr>
               </tbody>
           </table>
        </div>
    </div>
</div>
