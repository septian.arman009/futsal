@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transkasi</li>
    <li>Daftar Tagihan Member</li>
    <li class="active">Detail</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Tagihan Member</h3>
                </div>

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('tagihan.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    @include('admin.pembayaran._detailForm')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection