<a title="Edit" class="btn btn-info btn-xs waves-effect" href="{{ $edit_url }}"><i class="fa fa-pencil-square-o"></i> Ganti Jam</a>
<a title="Delete" class="btn btn-danger btn-xs waves-effect form-delete" id = "form-{{$id}}" data-url="{{$destroy_url}}" data-confirm = "{{$confirm_message}}"><i class="fa fa-trash-o"></i> Cancel</a>
