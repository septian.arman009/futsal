<button id="btn-c" style="display:none" class="btn btn-default mb-control" data-box="#modal-confirm"></button>
<div class="message-box message-box-warning animated fadeIn" id="modal-confirm">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-warning"></span> Confirmation</div>
			<div class="mb-content">
				<p id="message-btn-c"></p>
			</div>
			<div class="mb-footer">
				<button id="yes-modal-confirm" class="btn btn-success btn-lg mb-control-close">Yes</button>
				<button onclick="resetModal()" class="btn btn-default btn-lg mb-control-close">No</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-s" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Success </div>
			<div class="mb-content">
				<p id="message-btn-s"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-e" style="display:none" class="btn btn-default mb-control" data-box="#message-box-danger"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-danger">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<p id="message-btn-e"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-e-array" style="display:none" class="btn btn-default mb-control" data-box="#message-box-danger-array"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-danger-array">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<div id="message-btn-e-array"><ul></ul></div>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out ?</p>
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    {!! Form::open(['url' => route('logout'), 'method' => 'POST']) !!}
                    <button type="submit" class="btn btn-success btn-lg">Yes</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
