<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ $title }}</title>
    <meta http-equiv=" Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('bola.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('admin/css/theme-default.css') }}" />

</head>

<body>
    <div class="page-container page-navigation-top-fixed">
        <div class="page-sidebar page-sidebar-fixed scroll">

            <ul class="x-navigation">

                <li class="xn-logo">
                    <a>Futsal</a>
                    <a class="x-navigation-control"></a>
                </li>

                <li class="xn-profile">
                    <a class="profile-mini">
                        <img src="{{ asset('futsal.png') }}" alt="Avatar">
                    </a>
                    <div class="profile">
                        <div class="profile-image">
                            <img style="border:none" src="{{ asset('futsal.png') }}" alt="Avatar">
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name">{{ Auth::user()->name }}</div>
                            <div class="profile-data-title">{{ Auth::user()->email }}</div>
                            @role('member')
                            <a href="{{ route('change_password')}}" class="profile-control-right"><span
                                class="fa fa-lock"></span> Ganti Password</a>
                            @endrole
                        </div>
                        <div class="profile-controls">
                            @role('admin')
                            <a href="{{ url('admin/pembayaran/laporan') }}" class="profile-control-left"><span
                                    class="fa fa-bar-chart-o"></span></a>
                            
                            <a href="{{ route('change_password')}}" class="profile-control-right"><span
                                    class="fa fa-lock"></span></a>
                            @endrole
                        </div>
                    </div>
                </li>

                <li class="xn-title">Versi 1.0.0</li>
                {!! Html::mainMenu('home', url('admin/home'), 'fa fa-home', 'Beranda') !!}

                @role('admin')
                <li class="xn-openable pointer {{ set_active('master') }}">
                    <a>
                        <span class="fa fa-hdd-o"></span>
                        <span class="xn-text">Data Master</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('users', route('users.index'), 'fa fa-user', 'Pengguna') !!}
                        {!! Html::subMenu('lapangan', route('lapangan.index'), 'fa fa-flag', 'Lapangan') !!}
                        {!! Html::subMenu('harga', route('harga.index'), 'fa fa-money', 'Harga') !!}
                    </ul>
                </li>

                <li class="xn-openable pointer {{ set_active('transaksi') }}">
                    <a>
                        <span class="fa fa-random"></span>
                        <span class="xn-text">Transaksi</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('booking', route('booking.create', date('Y-m-d')), 'fa fa-book', 'Booking Lapangan') !!}
                        {!! Html::subMenu('booking_list', route('booking.index'), 'fa fa-list-alt', 'Daftar Booking') !!}
                        {!! Html::subMenu('tagihan', route('tagihan.index'), 'fa fa-money', 'Daftar Tagihan Member') !!}
                    </ul>
                </li>

                <li class="xn-openable pointer {{ set_active('pembayaran') }}">
                    <a>
                        <span class="fa fa-money"></span>
                        <span class="xn-text">Pembayaran</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('laporan', route('pembayaran.laporan', date('Y')), 'fa fa-bar-chart-o', 'Laporan Pembayaran') !!}
                    </ul>
                </li>
                @endrole

            </ul>
        </div>

        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right">
                    <a class="mb-control pointer" data-box="#mb-signout">
                        <span class="fa fa-sign-out"></span>
                    </a>
                </li>
            </ul>
            @yield('content')
        </div>
        @include('admin.layouts._notif')
        @include('admin.layouts._modal')
    </div>

    <audio id="audio-alert" src="{{ asset('admin/audio/alert.mp3') }}" preload="auto"></audio>
    <audio id="audio-fail" src="{{ asset('admin/audio/fail.mp3') }}" preload="auto"></audio>

    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}">
    </script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/morris/raphael-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/morris/morris.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/actions.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

    @yield('style')
    @yield('scripts')
</body>

<style>
    .pointer {
        cursor: pointer;
    }

    .row-bg {
        background: #f5f5f5 url('{{ url("admin/img/bg.png") }}') no-repeat;
    }

    .x-navigation>li.xn-logo>a:first-child {
        font-size: 25px;
        border-bottom: 0px;
        color: #FFF;
        height: 50px;
        text-align: center;
        background: #5c7572;
    }

    .x-navigation li.active>a {
        background: #5c7572;
        color: #fff;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease;
    }

    #datepicker {
        color: black;
        cursor: pointer;
    }
</style>

</html>