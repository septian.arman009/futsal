@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li>Lapangan</li>
    <li class="active">Edit</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Lapangan</h3>
                </div>
                
                {!! Form::model($lapangan, ['url' => route('lapangan.update', $lapangan->id),
                    'class' => 'form-horizontal form-save',
                    'method' =>'put',
                    'data-btn' => 'btn-save',
                    'data-redirect' => route('lapangan.index')]) !!}

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('lapangan.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.lapangan._form')
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection