<div class="form-group group-nama">
    {!! Form::label('nama', 'Nama', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-flag"></span>
            </span>
            {!! Form::text('nama', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>