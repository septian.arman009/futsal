@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Lapangan</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Lapangan</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('lapangan.create') }}">Tambah Lapangan</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="lapangan" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nama Lapangan</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Lapangan', 'id' => 'nama']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#lapangan').DataTable({
            processing: false,
            serverSide: true,
            order: [ [0, 'desc'] ],
            ajax: {
                url: "{{ route('lapangan.index') }}",
                data: function (d) {
                    d.name = $('#nama').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#nama').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #lapangan_filter {
        display: none;
    }
    #lapangan_length {
        width: 100%;
    }
</style>
@endsection