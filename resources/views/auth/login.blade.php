@extends('auth.layouts.app')

@section('content')
<div class="login-box animated fadeInDown">
    <div class="login-body">
        <div class="login-title"><strong>Welcome</strong>, Please login</div>
        @if (session('status') || $errors->has('email') || $errors->has('password'))
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            {{ session('status') }}
            {{ $errors->first('email', ':message')}}
            {{ $errors->first('password', ':message')}}
        </div>
        @endif  
        {!! Form::open(['url' => route('login'), 'class' => 'form-horizontal', 'method' => 'POST']) !!}
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <div class="col-md-12">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <div class="col-md-12">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6">
                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="btn btn-link btn-block">Forgot your password ?</a>
                @endif
            </div>
            <div class="col-md-6">
                <button class="btn btn-primary btn-block">Log In</button>
            </div>
        </div>
        {!! Form::close()!!}

    </div>
    <div class="login-footer">
        <div class="pull-left">
            &copy; 2019 Futsal
        </div>
        <div class="pull-right">
        </div>
    </div>
</div>
@endsection