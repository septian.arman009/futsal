<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <title>Futsal - Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="{{ asset('bola.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('admin/css/theme-default.css') }}" />
</head>

<body>

    <div class="login-container">

        @yield('content');

    </div>
    
    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
</body>

<style>
    .btn-primary {
        background-color: #5c7572;
        border-color: #fff;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open>.dropdown-toggle.btn-primary {
        background-color: #5c7572;
        border-color: #fff;
    }

    .login-container .login-box .login-body {
        width: 100%;
        float: left;
        background: rgba(255, 255, 255, 0.1);
        padding: 20px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        border-radius: 0px;
        border-color: #fff;
    }

    .login-body {
        border: 1px solid #fff;
    }
</style>

</html>