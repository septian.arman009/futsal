<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ $title }}</title>
    <meta http-equiv=" Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('bola.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('admin/css/theme-default.css') }}" />

</head>

<body>

    <div class="page-content">
        <div class="page-content-wrap">

            <div class="row">
                <div class="col-md-12">

                    <div class="error-container">
                        <div class="error-code">404</div>
                        <div class="error-text">Halaman tidak ditemukan</div>
                        <div class="error-subtext">Sayang sekali halaman yang anda minta tidak kami temukan,
                            silakan hubungi
                            admin untuk info selanjutnya.</div>
                        <div class="error-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{  url('/login') }}" class="btn btn-info btn-block btn-lg">Kembali ke
                                        login</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ URL::previous() }}" class="btn btn-primary btn-block btn-lg">Halaman
                                        sebelumnya</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</body>

</html>