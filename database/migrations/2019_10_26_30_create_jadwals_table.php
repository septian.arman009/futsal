<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->integer('lapangan_id')->length(10)->unsigned();
            $table->string('hari', 10);
            $table->integer('waktu_awal')->length(10);
            $table->integer('waktu_akhir')->length(10);
            $table->integer('durasi')->length(10);
            $table->string('status', 25);
            $table->timestamps();
            
            $table->foreign('lapangan_id')->references('id')->on('lapangans')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}
