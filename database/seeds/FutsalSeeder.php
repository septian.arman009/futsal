<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Booking;
use App\Harga;
use App\Jadwal;
use App\Lapangan;

class FutsalSeeder extends Seeder
{
    public function run()
    {
        //Lapangan
        $lapangan_01 = Lapangan::create(['nama' => 'Lapangan 1']);
        $lapangan_02 = Lapangan::create(['nama' => 'Lapangan 2']);
        $lapangan_03 = Lapangan::create(['nama' => 'Lapangan 3']);
        $lapangans = array($lapangan_01->id, $lapangan_02->id, $lapangan_03->id);
       
        //Harga
        $haris = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        $margin = 0;
        foreach ($lapangans as $l_key => $lapangan) {
            foreach ($haris as $h_key => $hari) {
                if($h_key <= 4){
                    $harga_01 = 100000;
                    $harga_02 = 130000;
                }else{
                    $harga_01 = 130000;
                    $harga_02 = 150000;
                }

                $harga[] = array(
                    'lapangan_id' => $lapangan,
                    'hari' => $hari,
                    'waktu_awal' => 8,
                    'waktu_akhir' => 17,
                    'harga' => $harga_01 + $margin,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $harga[] = array(
                    'lapangan_id' => $lapangan,
                    'hari' => $hari,
                    'waktu_awal' => 18,
                    'waktu_akhir' => 22,
                    'harga' => $harga_02 + $margin,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                //Jadwal
                $awal = random_int(16, 17);
                $akhir = random_int(19, 20);

                $jadwal[] = array(
                    'lapangan_id' => $lapangan,
                    'hari' => $hari,
                    'waktu_awal' => $awal,
                    'waktu_akhir' => $akhir,
                    'durasi' => $akhir - $awal,
                    'status' => 'Booked',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
            }
            $margin += 10000;
        }
        
        Harga::insert($harga);
        Jadwal::insert($jadwal);

        $jadwal_id = 1;
        foreach ($lapangans as $l_key => $lapangan) { 
            for ($i=1; $i <= 7; $i++) {

                $jb = Jadwal::find($jadwal_id);
                $waktu_awal = $jb->waktu_awal;
                $waktu_akhir = $jb->waktu_akhir;
                $durasi = $jb->durasi;

                $tagihan = 0;
                for ($j=0; $j < $durasi ; $j++) { 
                    $harga = Harga::where([
                        ['lapangan_id', $jb->lapangan_id],
                        ['hari', $jb->hari],
                        ['waktu_awal', '<=', $waktu_awal+$j],
                        ['waktu_akhir', '>=', $waktu_awal+$j]
                    ])->first()->harga;

                    $tagihan += $harga;
                }

                $booking[] = array(
                    'jadwal_id' => $jadwal_id,
                    'user_id' => $i+1,
                    'tagihan' => $tagihan,
                    'status' => 'Booked',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $jadwal_id++;
            }
        }

        Booking::insert($booking);

    }
}
