<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Admin';
        $adminRole->save();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'septian.arman009@gmail.com';
        $admin->password = bcrypt('januari1993');
        $admin->is_verified = 1;
        $admin->save();
        $admin->attachRole($adminRole);

        $adminRole_01 = new Role();
        $adminRole_01->name = 'member';
        $adminRole_01->display_name = 'Member';
        $adminRole_01->save();

        
        $member = new User();
        $member->name = 'Naruto';
        $member->email = 'naruto@gmail.com';
        $member->password = bcrypt('januari1993');
        $member->is_verified = 1;
        $member->save();
        $member->attachRole($adminRole_01);

        $member_01 = new User();
        $member_01->name = 'Sasuke';
        $member_01->email = 'sasuke@gmail.com';
        $member_01->password = bcrypt('januari1993');
        $member_01->is_verified = 1;
        $member_01->save();
        $member_01->attachRole($adminRole_01);

        $member_02 = new User();
        $member_02->name = 'Sakura';
        $member_02->email = 'sakura@gmail.com';
        $member_02->password = bcrypt('januari1993');
        $member_02->is_verified = 1;
        $member_02->save();
        $member_02->attachRole($adminRole_01);

        $member_03 = new User();
        $member_03->name = 'Jiraya';
        $member_03->email = 'jiraya@gmail.com';
        $member_03->password = bcrypt('januari1993');
        $member_03->is_verified = 1;
        $member_03->save();
        $member_03->attachRole($adminRole_01);

        $member_04 = new User();
        $member_04->name = 'Uchia';
        $member_04->email = 'uchia@gmail.com';
        $member_04->password = bcrypt('januari1993');
        $member_04->is_verified = 1;
        $member_04->save();
        $member_04->attachRole($adminRole_01);

        $member_05 = new User();
        $member_05->name = 'Hinata';
        $member_05->email = 'hinata@gmail.com';
        $member_05->password = bcrypt('januari1993');
        $member_05->is_verified = 1;
        $member_05->save();
        $member_05->attachRole($adminRole_01);

        $member_06= new User();
        $member_06->name = 'Sunade';
        $member_06->email = 'sunade@gmail.com';
        $member_06->password = bcrypt('januari1993');
        $member_06->is_verified = 1;
        $member_06->save();
        $member_06->attachRole($adminRole_01);

    }
}
