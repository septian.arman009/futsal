<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateVerificationToken()
    {
        $token = $this->verification_token;
        if(!$token){
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }
        return $token;
    }

    public function sendVerification()
    {
        $token = $this->generateVerificationToken();
        $user = $this;
        Mail::send('auth.emails.verification', compact('user', 'token'), function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Futsal verification email');
        });
    }

    public function verify()
    {
        $this->email_verified_at = date('Y-m-d H:i:s');
        $this->is_verified = 1;
        $this->verification_token  = null;
        $this->save();
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
}
