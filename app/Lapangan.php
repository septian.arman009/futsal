<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lapangan extends Model
{
    protected $fillable = ['nama'];

    public function harga()
    {
        return $this->hasMany('App\Harga');
    }

    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }
}
