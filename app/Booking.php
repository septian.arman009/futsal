<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['jadwal_id', 'user_id', 'tagihan', 'status', 'created_at', 'updated_at'];

    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
