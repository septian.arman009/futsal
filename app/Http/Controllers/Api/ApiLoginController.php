<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Response;

class ApiLoginController extends Controller
{
    public function login(Request $request){
       $data = $request->all();
       $user = User::where([
           'email' => $data['email'],
           'password' => bcrypt($data['password'])
        ]);

       if($user){
           echo 'login';
       }
    }
}
