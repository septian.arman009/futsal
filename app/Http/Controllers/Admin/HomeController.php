<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Http\Controllers\Controller;
use Laratrust\LaratrustFacade as Laratrust;
use Auth;
use App\Lapangan;
use Illuminate\Http\Request;
use Datatables;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user-should-verified');
    }

    public function index(Request $request)
    {
        if (Laratrust::hasRole('admin')) {
            return $this->adminDashboard();
        }

        if (Laratrust::hasRole('member')) {
            return $this->memberDashboard($request);
        }

    }

    public function adminDashboard()
    {
        $date = explode('-', date('Y-m-d'));
        $jumlah_hari = $number = cal_days_in_month(CAL_GREGORIAN, $date[1], $date[0]);

        $booking = Booking::whereMonth('created_at', $date[1])
            ->whereYear('created_at', $date[0])
            ->where('status', 'Lunas')->get();

        $total_pendapatan = 0;
        $tagihan = 0;
        for ($i = 1; $i <= $jumlah_hari; $i++) {

            foreach ($booking as $key => $book) {
                $dtime = explode(' ', $book->created_at);
                $dt = explode('-', $dtime[0]);
                if ($dt[2] == $i) {
                    $tagihan += $book->tagihan;
                }
            }

            $data['bar'][] = [
                'y' => 'Tgl :' . $i,
                'a' => $tagihan,
            ];

            $total_pendapatan += $tagihan;
            $tagihan = 0;

        }

        $lapangan = Lapangan::all();
        foreach ($lapangan as $key => $_lapangan) {

            $jadwal_id = array();
            $durasi = 0;
            foreach ($_lapangan->jadwal->where('status', 'Lunas') as $key => $_lj) {
                $jadwal_id[] = $_lj->id;
                $durasi += $_lj->durasi;
            }

            $data['donut'][] = [
                'label' => $_lapangan->nama,
                'value' => $durasi,
            ];

            $data['color'][] = $this->rand_color();

            $data['lapangan'][] = [
                'nama' => $_lapangan->nama,
                'durasi' => $durasi,
                'total_pendapatan' => Booking::whereIn('jadwal_id', $jadwal_id)
                    ->where('status', 'Lunas')
                    ->sum('tagihan'),
            ];

        }

        $data['bulan'] = to_month($date[1]);
        $data['tahun'] = $date[0];
        $data['total_pendapatan'] = torp($total_pendapatan);

        $data['title'] = 'Admin Dashboard';
        return view('admin/home/admin', $data);
    }

    public function memberDashboard($request)
    {
        if ($request->ajax()) {
            $bookings = Booking::where('user_id', Auth::user()->id)->get();
            return Datatables::of($bookings)
                ->addColumn('member', function ($booking) {
                    return $booking->user->name;
                })
                ->addColumn('tanggal', function ($booking) {
                    return $booking->jadwal->hari.', '.indoFullDateDay($booking->created_at);
                })
                ->addColumn('lapangan', function ($booking) {
                    return $booking->jadwal->lapangan->nama;
                })
                ->addColumn('jam_masuk', function ($booking) {
                    return futsalTime($booking->jadwal->waktu_awal);
                })
                ->addColumn('jam_keluar', function ($booking) {
                    return futsalTime($booking->jadwal->waktu_akhir);
                })
                ->addColumn('tagihan', function ($booking) {
                    return torp($booking->tagihan);
                })
                ->rawColumns(['member', 'tanggal', 'lapangan', 'jam_masuk', 'jam_keluar', 'tagihan'])
                ->make(true);
        }

        $data['title'] = 'Member Dashboard';
        return view('admin.home.member', $data);
    }

    public function rand_color()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }
}
