<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\Rules\MatchOldPassword;
use App\User;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::select(['id', 'name', 'email'])->get();
            return Datatables::of($users)
                ->addColumn('role', function ($user) {
                    return ucfirst($user->roles[0]->name);
                })
                ->addColumn('action', function ($user) {
                    return view('admin.datatables._action', [
                        'id' => $user->id,
                        'destroy_url' => route('users.destroy', $user->id),
                        'edit_url' => route('users.edit', $user->id),
                        'confirm_message' => 'Apakah anda yakin ingin menghapus pengguna ' . $user->name . ' ?',
                    ]);
                })
                ->rawColumns(['role', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['name'], $request->get('name')) ? true : false;
                        });
                    }
                    if (!empty($request->get('email'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['email'], $request->get('email')) ? true : false;
                        });
                    }
                    if (!empty($request->get('role'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['role'], $request->get('role')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Pengguna';
        return view('admin.user.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Pengguna';
        return view('admin.user.create', $data);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:8',
        ]);

        if ($validator->passes()) {
            $data = $request->except('role_id');
            $data['password'] = bcrypt($data['password']);

            $user = User::create($data);
            $role = Role::where('id', $request->role_id)->first();
            $user->attachRole($role);
            $user->sendVerification();
            return response()->json(['success' => 'Berhasil menambahkan Pengguna']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit Pengguna';
        $user = User::find($id);
        return view('admin.user.edit')->with(compact('user', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email,' . $id,
            'password' => 'nullable',
        ]);

        if ($validator->passes()) {
            $user = User::find($id);
            $data = $request->except('role_id');
            $user->update($data);
            $user->roles()->sync([$request->role_id]);
            return response()->json(['success' => 'Berhasil mengubah data Pengguna']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }

    }

    public function destroy($id)
    {
        if (Auth::user()->id != $id) {
            $user = User::where('id', $id)->delete();
            return response()->json(['id' => $id]);
        } else {
            return response()->json(['error' => 'Gagal menghapus Pengguna']);
        }
    }

    public function changePassword()
    {
        $data['title'] = 'Ganti Password';
        return view('admin.user.change_password', $data);
    }

    public function storeNewPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required',
            'new_confirm_password' => 'required|same:new_password|min:8',
        ]);

        if ($validator->passes()) {
            User::find(Auth::user()->id)->update(['password' => Hash::make($request->new_password)]);
            return response()->json(['success' => 'Berhasil mengubah data Pengguna']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }
}
