<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\Lapangan;
use Validator;

class LapanganController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $lapangans = Lapangan::select(['id', 'nama'])->get();
            return Datatables::of($lapangans)
                ->addColumn('action', function ($lapangan) {
                    return view('admin.datatables._action', [
                        'id' => $lapangan->id,
                        'destroy_url' => route('lapangan.destroy', $lapangan->id),
                        'edit_url' => route('lapangan.edit', $lapangan->id),
                        'confirm_message' => 'Apakah anda yakin ingin menghapus lapangan ' . $lapangan->nama . ' ?',
                    ]);
                })
                ->rawColumns(['action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('nama'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['nama'], $request->get('nama')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Lapangan';
        return view('admin.lapangan.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Lapangan';
        return view('admin.lapangan.create', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:lapangans'
        ]);

        if ($validator->passes()) {
            $lapangan = Lapangan::create($request->all());
            return response()->json(['success' => 'Berhasil menambahkan Lapangan']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Lapangan';
        $data['lapangan'] = Lapangan::find($id);
        return view('admin.lapangan.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:lapangans,nama,' . $id,
        ]);

        if ($validator->passes()) {
            $lapangan = Lapangan::find($id);
            $lapangan->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Lapangan']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }

    }

    public function destroy($id)
    {
        $lapangan = Lapangan::where('id', $id);
        if($lapangan->first()->harga->count() > 0){
            return response()->json(['error' => 'Lapangan tidak dapat dihapus karena memiliki telah data harga']);
        }else{
            $lapangan->delete();
            return response()->json(['id' => $id]);
        }
    }
}
