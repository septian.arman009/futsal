<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Harga;
use App\Http\Controllers\Controller;
use App\Jadwal;
use App\Lapangan;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Role;
use Datatables;
use Illuminate\Support\Str;

class BookingController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $bookings = Booking::select(['id', 'jadwal_id', 'user_id', 'tagihan', 'status'])->where('status', 'Booked')->get();
            return Datatables::of($bookings)
                ->addColumn('member', function ($booking) {
                    return $booking->user->name;
                })
                ->addColumn('lapangan', function ($booking) {
                    return $booking->jadwal->lapangan->nama;
                })
                ->addColumn('date', function ($booking) {
                    return $booking->jadwal->hari.', '.indoFullDateDay($booking->jadwal->created_at);
                })
                ->addColumn('waktu_awal', function ($booking) {
                    return futsalTime($booking->jadwal->waktu_awal);
                })
                ->addColumn('waktu_akhir', function ($booking) {
                    return futsalTime($booking->jadwal->waktu_akhir);
                })
                ->addColumn('tagihan', function ($booking) {
                    return torp($booking->tagihan);
                })
                ->addColumn('action', function ($booking) {
                    return view('admin.datatables._action_booking', [
                        'id' => $booking->id,
                        'destroy_url' => route('booking.destroy', $booking->id),
                        'edit_url' => route('booking.edit', $booking->id),
                        'confirm_message' => 'Apakah anda yakin ingin cancel booking atas nama member ' . $booking->user->name . ' ?',
                    ]);
                })
                ->rawColumns(['member', 'lapangan', 'date', 'waktu_awal', 'waktu_akhir', 'tagihan', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('member'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['member'], $request->get('member')) ? true : false;
                        });
                    }
                    if (!empty($request->get('lapangan'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['lapangan'], $request->get('lapangan')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['date'], $request->get('date')) ? true : false;
                        });
                    }
                    if (!empty($request->get('tagihan'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['tagihan'], $request->get('tagihan')) ? true : false;
                        });
                    }
                    if (!empty($request->get('status'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['status'], $request->get('status')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Booking';
        return view('admin.booking.index', $data);
    }

    public function create($date)
    {
        $hari = namaHari($date);

        $lapangan = Lapangan::all();
        $jamAktif = array();

        foreach ($lapangan as $_lapangan) {
            $list_harga = Harga::where([
                ['lapangan_id', $_lapangan->id],
                ['hari', $hari],
            ]);

            $min[] = $list_harga->min('waktu_awal');
            $max[] = $list_harga->max('waktu_akhir');

            $jadwalBooked = Jadwal::where([
                ['lapangan_id', $_lapangan->id],
                ['hari', $hari],
            ])
            ->whereDate('created_at', '=', $date)
            ->get();

            $jam = array();
            if ($jadwalBooked) {

                $id = array();
                foreach ($jadwalBooked as $key => $jb) {
                    for ($i = $jb->waktu_awal; $i <= $jb->waktu_akhir - 1; $i++) {
                        $jam[] = $i;
                    }
                    $id[] = $jb->id;
                }

                $jamAktif[] = $jam;

                $memberBooked[] = Booking::whereIn('jadwal_id', $id)->get()->count();

            } else {
                $memberBooked[] = 0;
                $jamAktif[] = array();
            }
        }

        $data['lapangan'] = $lapangan;
        $data['min'] = $min;
        $data['max'] = $max;
        $data['hari'] = $hari;
        $data['date'] = $date;
        $data['indoFullDate'] = indoFullDate($date);
        $data['memberBooked'] = $memberBooked;
        $data['jamAktif'] = $jamAktif;
        $data['title'] = 'Booking Lapangan';
        return view('admin.booking.create', $data);

    }

    public function formBooking(Request $request)
    {
        $waktu_awal = $request->waktu_awal;
        $lapangan_id = $request->lapangan_id;
        $hari = $request->hari;
        $date = $request->date;

        $detail = Jadwal::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
            ['status', 'Booked']
        ])
            ->whereDate('created_at', '=', $date)
            ->get();

        $maxPlay = Harga::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
        ])->max('waktu_akhir');

        $jam = array('- Pilih Durasi Main -');
        $durasi = 1;
        for ($i = $waktu_awal + 1; $i <= $maxPlay; $i++) {
            $jam[$durasi] = $durasi . ' Jam';
            $durasi++;
        }

        $data['details'] = $detail;
        $data['waktu_awal'] = $waktu_awal;
        $data['lapangan_id'] = $lapangan_id;
        $data['hari'] = $hari;
        $data['date'] = $date;
        $data['jam'] = $jam;
        $data['title'] = "Form Booking Lapangan";
        $data['members'] = Role::where('name', 'member')->first()->users()->get()->pluck('name', 'id')->prepend('- Pilih Member -','');
        return view('admin.booking.form_booking', $data);
    }

    public function store(Request $request)
    {
        $user_id = $request->user_id;
        $waktu_awal = $request->waktu_awal;
        $durasi = $request->durasi;
        $lapangan_id = $request->lapangan_id;
        $hari = $request->hari;
        $date = $request->date;

        $validator = Validator::make($request->all(), [
            'durasi' => 'gt:0',
        ], [
            'durasi.gt' => 'Minimal main 1 jam bro',
        ]);

        if ($validator->passes()) {
            $detail = Jadwal::where([
                ['lapangan_id', $lapangan_id],
                ['hari', $hari],
                ['status', 'Booked']
            ])
                ->whereDate('created_at', '=', $date)
                ->get();

            $jam = array();
            foreach ($detail as $key => $dtl) {
                for ($i = $dtl->waktu_awal; $i <= $dtl->waktu_akhir - 1; $i++) {
                    $jam[] = $i;
                }
            }

            $tagihan = 0;
            for ($j = $waktu_awal; $j <= ($waktu_awal + $durasi) - 1; $j++) {

                if (in_array($j, $jam)) {
                    $error = [
                        'error' => ["Jam " . futsalTime($j) . " sudah disewa"],
                    ];
                    return response()->json(['error' => $error]);
                    die();
                }

                $harga = Harga::where([
                    ['lapangan_id', $lapangan_id],
                    ['hari', $hari],
                    ['waktu_awal', '<=', $j],
                    ['waktu_akhir', '>=', $j],
                ]);

                if ($harga->count() > 0) {
                    $tagihan += $harga->first()->harga;
                } else {
                    $error = [
                        'error' => ["Jam " . futsalTime($j) . " tidak memiliki harga / tidak ada jadwal"],
                    ];
                    return response()->json(['error' => $error]);
                    die();
                }

            }

            $datajadwal = array(
                'lapangan_id' => $lapangan_id,
                'hari' => $hari,
                'waktu_awal' => $waktu_awal,
                'waktu_akhir' => $waktu_awal + $durasi,
                'durasi' => $durasi,
                'status' => 'Booked',
                'created_at' => $date.date(' H:i:s'),
                'updated_at' => $date.date(' H:i:s')
            );

            $jadwal = Jadwal::create($datajadwal);

            $dataBooking = array(
                'jadwal_id' => $jadwal->id,
                'user_id' => $user_id,
                'tagihan' => $tagihan,
                'status' => 'Booked',
                'created_at' => $date.date(' H:i:s'),
                'updated_at' => $date.date(' H:i:s')
            );

            $booking = Booking::create($dataBooking);

            return response()->json(['success' => 'Booking berhasil']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }

    }

    public function edit($id)
    {
        $booking = Booking::where('id', $id)->first();

        $lapangan_id = $booking->jadwal->lapangan_id;
        $hari = $booking->jadwal->hari;
        $date = takeDate($booking->jadwal->created_at);

        $lapangan = Lapangan::where('id', $lapangan_id)->get();

        $list_harga = Harga::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
        ]);

        $min = $list_harga->min('waktu_awal');
        $max = $list_harga->max('waktu_akhir');

        $jadwalBooked = Jadwal::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
            ['status', 'Booked']
        ])
        ->whereDate('created_at', '=', $date)
        ->get();

        $jam = array();
        if ($jadwalBooked) {

            $id = array();
            foreach ($jadwalBooked as $key => $jb) {
                for ($i = $jb->waktu_awal; $i <= $jb->waktu_akhir - 1; $i++) {
                    $jam[] = $i;
                }
                $id[] = $jb->id;
            }
        }

        $jamMember = array();
        for ($i=$booking->jadwal->waktu_awal; $i < $booking->jadwal->waktu_akhir ; $i++) { 
            $jamMember[] = $i;
        }

        $waktu_awal = $booking->jadwal->waktu_awal;
        $waktu_akhir = $booking->jadwal->waktu_akhir;
        $data['waktu_member'] = futsalTime($waktu_awal)." - ".futsalTime($waktu_akhir);
        $data['member'] = $booking->user->name;

        $data['booking_id'] = $booking->id;
        $data['jadwal_id'] = $booking->jadwal->id;
        $data['lapangan'] = $lapangan;
        $data['min'] = $min;
        $data['max'] = $max;
        $data['hari'] = $hari;
        $data['date'] = $date;
        $data['indoFullDate'] = indoFullDate($date);
        $data['jam'] = $jam;
        $data['jamMember'] = $jamMember;
        $data['title'] = "Edit Jam Booking";
        return view('admin.booking.edit', $data);
    }

    public function formBookingEdit(Request $request)
    {
        $booking_id = $request->booking_id;
        $jadwal_id = $request->jadwal_id;
        $waktu_awal = $request->waktu_awal;
        $lapangan_id = $request->lapangan_id;
        $hari = $request->hari;
        $date = $request->date;

        $detail = Jadwal::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
            ['status', 'Booked']
        ])
        ->whereDate('created_at', '=', $date)
        ->get();

        $maxPlay = Harga::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
        ])->max('waktu_akhir');

        $jam = array('- Pilih Durasi Main -');
        $durasi = 1;
        for ($i = $waktu_awal + 1; $i <= $maxPlay; $i++) {
            $jam[$durasi] = $durasi . ' Jam';
            $durasi++;
        }

        $data['booking_id'] = $booking_id;
        $data['jadwal_id'] = $jadwal_id;
        $data['details'] = $detail;
        $data['waktu_awal'] = $waktu_awal;
        $data['lapangan_id'] = $lapangan_id;
        $data['hari'] = $hari;
        $data['date'] = $date;
        $data['jam'] = $jam;
        $data['title'] = "Jam Booking Baru";
        return view('admin.booking.form_booking_edit', $data);
    }

    public function update(Request $request, $booking_id)
    {   
        $jadwal_id = $request->jadwal_id;
        $waktu_awal = $request->waktu_awal;
        $durasi = $request->durasi;
        $lapangan_id = $request->lapangan_id;
        $hari = $request->hari;
        $date = $request->date;

        $validator = Validator::make($request->all(), [
            'durasi' => 'gt:0',
        ], [
            'durasi.gt' => 'Minimal main 1 jam bro',
        ]);

        if ($validator->passes()) {
            $detail = Jadwal::where([
                ['lapangan_id', $lapangan_id],
                ['hari', $hari],
                ['status', 'Booked'],
                ['id', '!=', $jadwal_id]
            ])
            ->whereDate('created_at', '=', $date)
            ->get();

            $jam = array();
            foreach ($detail as $key => $dtl) {
                for ($i = $dtl->waktu_awal; $i <= $dtl->waktu_akhir - 1; $i++) {
                    $jam[] = $i;
                }
            }

            $tagihan = 0;
            for ($j = $waktu_awal; $j <= ($waktu_awal + $durasi) - 1; $j++) {

                if (in_array($j, $jam)) {
                    $error = [
                        'error' => ["Jam " . futsalTime($j) . " sudah disewa"],
                    ];
                    return response()->json(['error' => $error]);
                    die();
                }

                $harga = Harga::where([
                    ['lapangan_id', $lapangan_id],
                    ['hari', $hari],
                    ['waktu_awal', '<=', $j],
                    ['waktu_akhir', '>=', $j],
                ]);

                if ($harga->count() > 0) {
                    $tagihan += $harga->first()->harga;
                } else {
                    $error = [
                        'error' => ["Jam " . futsalTime($j) . " tidak memiliki harga / tidak ada jadwal"],
                    ];
                    return response()->json(['error' => $error]);
                    die();
                }

            }

            $datajadwal = array(
                'lapangan_id' => $lapangan_id,
                'hari' => $hari,
                'waktu_awal' => $waktu_awal,
                'waktu_akhir' => $waktu_awal + $durasi,
                'durasi' => $durasi,
            );

            $jadwal = Jadwal::where('id', $jadwal_id)->update($datajadwal);

            $dataBooking = array(
                'tagihan' => $tagihan,
                'status' => 'Booked',
            );

            $booking = Booking::where('id', $booking_id)->update($dataBooking);

            return response()->json(['success' => 'Update jam booking berhasil']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }

    }

    public function destroy($id)
    {
        $booking = Booking::where('id', $id);
        $jadwal = Jadwal::where('id', $booking->first()->id)->delete();
        $booking->delete();
        return response()->json(['id' => $id]);
    }
}
