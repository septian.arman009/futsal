<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PembayaranController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $members = Role::where('name', 'member')->first()->users()->whereHas('booking', function ($q) {
                $q->where('status', 'Booked');
                $q->orWhere('status', 'Pending');
            })->get();
            return Datatables::of($members)
                ->addColumn('tagihan', function ($member) {
                    return torp($member->booking
                    ->where('status', 'Booked')
                    ->sum('tagihan'));
                })
                ->addColumn('pending', function ($member) {
                    return torp($member->booking
                    ->where('status', 'Pending')
                    ->sum('tagihan'));
                })
                ->addColumn('action', function ($member) {
                    return '<a title="Detail Tagihan Member"
                                class="btn btn-info btn-xs waves-effect"
                                href="' . route('tagihan.detail', $member->id) . '">
                                <i class="fa fa-eye"></i> Detail Tagihan </a>';
                })
                ->rawColumns(['tagihan', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['name'], $request->get('name')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Tagihan Member';
        return view('admin.pembayaran.tagihan', $data);
    }

    public function detailTagihan($user_id)
    {
        $bookings = Booking::where('user_id', $user_id);
        $tagihan = torp($bookings ->where('status', 'Booked')->orWhere('status', 'Pending')->where('user_id', $user_id)->sum('tagihan'));

        $data['user_id'] = $user_id;
        $data['tagihan'] = $tagihan;
        $data['total_booking'] = $bookings->count();
        $data['bookings'] = $bookings->where('status', 'Booked')->orWhere('status', 'Pending')->where('user_id', $user_id)->get();
        $data['member'] = User::find($user_id);
        $data['title'] = 'Detail Tagihan Member';
        return view('admin.pembayaran.detailTagihan', $data);
    }

    public function store(Request $request)
    {
        $booking_id = $request->booking_id;

        $data = [
            'status' => 'Lunas',
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $booking = Booking::where('id', $booking_id)->first();

        $booking->update($data);
        $booking->jadwal->update($data);
        
        return response()->json(['success' => 'Pembayaran berhasil']);

    }

    public function koreksi(Request $request)
    {
        $booking_id = $request->booking_id;

        $data = [
            'status' => 'Pending',
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $booking = Booking::where('id', $booking_id)->first();

        $booking->update($data);
        $booking->jadwal->update($data);

        return response()->json(['success' => 'Pembayaran berhasil dipending']);

    }

    public function laporanPembayaran(Request $request, $tahun)
    {
        if ($request->ajax()) {
            $members = Role::where('name', 'member')->first()->users()->whereHas('booking', function ($q) use ($tahun){
                $q->where('status', 'Lunas')->whereYear('updated_at', '=', $tahun);
                $q->orWhere('status', 'Lunas')->whereYear('updated_at', '=', $tahun);
            })->get();
            return Datatables::of($members)
                ->addColumn('tagihan', function ($member) {
                    return torp($member->booking->where('status', 'Lunas')->sum('tagihan'));
                })
                ->addColumn('pending', function ($member) {
                    $tagihan = $member->booking->where('status', 'Pending')->sum('tagihan');
                    if($tagihan > 0){
                        return '<p style="color: red;font-weight: bold">'.torp($tagihan).'</p>';

                    }else{
                        return torp($tagihan);

                    }
                })
                ->addColumn('date', function ($member) use ($request) {
                    if (!empty($request->get('date')) && !empty($request->get('tanggal'))) {
                        $search_date = $request->get('tanggal') . ' ' . $request->get('date');
                    }else if(!empty($request->get('date')) && empty($request->get('tanggal'))){
                        $search_date = $request->get('date');
                    }else if(empty($request->get('date')) && !empty($request->get('tanggal'))){
                        $search_date = $request->get('tanggal');
                    }else if (empty($request->get('date')) && empty($request->get('tanggal'))) {
                        $search_date = '';
                    }
                   
                    $html = '';
                    $old_date = '';
                    foreach ($member->booking as $mb) {
                        if(indoFullDateDay($mb->updated_at) != $old_date){
                            if($search_date != ''){
                               
                                if(strpos(indoFullDateDay($mb->updated_at), $search_date) !== false){
                                    $html .= "<p>" . indoFullDateDay($mb->updated_at) . "</p>";
                                    $old_date = indoFullDateDay($mb->updated_at);
                                }

                            }else{
                                $html .= "<p>" . indoFullDateDay($mb->updated_at) . "</p>";
                                $old_date = indoFullDateDay($mb->updated_at);
                            }
                        }
                    }

                    return $html;
                })
                ->addColumn('action', function ($member) use ($tahun){
                   
                    return '<a title="Detail Pembayarang Member"
                            class="btn btn-success btn-xs waves-effect"
                            href="' . route('pembayaran.detail', ['user_id' => $member->id, 'tahun' => $tahun]) . '">
                            <i class="fa fa-eye"></i> Detail Pembayaran....</a>';

                })
                ->rawColumns(['tagihan', 'pending', 'date', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['name'], $request->get('name')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date')) || !empty($request->get('tanggal'))) {
                        $search_date = $request->get('tanggal') . ' ' . $request->get('date');
                        $instance->collection = $instance->collection->filter(function ($row) use ($request, $search_date) {
                            return Str::contains($row['date'], $search_date) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $total_pendapatan = 0;

        for ($i = 1; $i <= 12; $i++) {
            $booking = Booking::whereMonth('created_at', $i)->whereYear('created_at', $tahun)->where('status', 'Lunas');
            if ($booking->count() > 0) {
                $tagihan = $booking->sum('tagihan');

                $data['bar'][] = [
                    'y' => to_month($i),
                    'a' => $tagihan,
                ];

                $total_pendapatan += $tagihan;
            } else {
                $data['bar'][] = [
                    'y' => to_month($i),
                    'a' => 0,
                ];
            }

        }

        $data['tahun'] = $tahun;
        $data['total_pendapatan'] = torp($total_pendapatan);

        $data['title'] = 'Laporan Pembayaran';
        return view('admin.pembayaran.laporan', $data);
    }

    public function detailPembayaran($user_id, $tahun)
    {
        $bookings = Booking::where('user_id', $user_id);
        $tagihan = $bookings->where('status', 'Lunas')->orWhere('status', 'Pending')->where('user_id', $user_id)->sum('tagihan');
        $total_bayar = $bookings->where('status' ,'Lunas')->sum('tagihan');

        $data['user_id'] = $user_id;
        $data['tagihan'] = torp($tagihan);
        $data['total_bayar'] = torp($total_bayar);
        $data['sisa'] = torp($tagihan - $total_bayar);
        $data['total_booking'] = $bookings->count();
        $data['bookings'] = $bookings->where('status', 'Lunas')->orWhere('status', 'Pending')->where('user_id', $user_id)->get();
        $data['member'] = User::find($user_id);
        $data['tahun'] = $tahun;
        $data['title'] = 'Detail Pembayaran Member';
        return view('admin.pembayaran.detailPembayaran', $data);
    }
}
