<?php

namespace App\Http\Controllers\Admin;

use App\Harga;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class HargaController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $hargas = Harga::select(['id', 'lapangan_id', 'hari', 'waktu_awal', 'waktu_akhir', 'harga'])->get();
            return Datatables::of($hargas)
                ->addColumn('lapangan', function ($harga) {
                    return $harga->lapangan->nama;
                })
                ->addColumn('waktu_awal', function ($harga) {
                    return futsalTime($harga->waktu_awal);
                })
                ->addColumn('waktu_akhir', function ($harga) {
                    return futsalTime($harga->waktu_akhir);
                })
                ->addColumn('harga', function ($harga) {
                    return torp($harga->harga);
                })
                ->addColumn('action', function ($harga) {
                    return view('admin.datatables._action', [
                        'id' => $harga->id,
                        'destroy_url' => route('harga.destroy', $harga->id),
                        'edit_url' => route('harga.edit', $harga->id),
                        'confirm_message' => 'Apakah anda yakin ingin menghapus Harga ' . $harga->nama . ' ?',
                    ]);
                })
                ->rawColumns(['lapangan', 'waktu_awal', 'waktu_akhir', 'harga', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('lapangan'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['lapangan'], $request->get('lapangan')) ? true : false;
                        });
                    }
                    if (!empty($request->get('hari'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['hari'], $request->get('hari')) ? true : false;
                        });
                    }
                    if (!empty($request->get('waktu_awal'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['waktu_awal'], $request->get('waktu_awal')) ? true : false;
                        });
                    }
                    if (!empty($request->get('waktu_akhir'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['waktu_akhir'], $request->get('waktu_akhir')) ? true : false;
                        });
                    }
                    if (!empty($request->get('harga'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['harga'], $request->get('harga')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Harga';
        return view('admin.harga.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Harga';
        $data['hari'] = hari();
        return view('admin.harga.create', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lapangan_id' => 'required',
            'hari' => 'required',
            'harga' => 'required',
        ]);

        if ($validator->passes()) {

            $batas_bawah = Harga::where([
                ['lapangan_id', $request->lapangan_id],
                ['hari', $request->hari]
            ])->latest('waktu_akhir');

            if($batas_bawah->count() > 0){
                $waktu_akhir = $batas_bawah->first()->waktu_akhir;
            }else{
                $waktu_akhir = 0;
            }

            if ($request->waktu_awal > $waktu_akhir && $request->waktu_akhir > $request->waktu_awal) {
                $harga = Harga::create($request->all());
                return response()->json(['success' => 'Berhasil menambahkan Lapangan']);
            } else {
                $error = [
                    'waktu_awal' => ['Silakan cek kembali pengaturan waktu, waktu awal harus lebih besar dari waktu akhir terakhir'],
                    'waktu_akhir' => ['Silakan cek kembali pengaturan waktu, waktu awal harus lebih besar dari waktu awal'],
                ];
                return response()->json(['error' => $error]);
            }

        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function lastRecord($lapangan_id, $hari)
    {
        $data['last_record'] = Harga::where([
            ['lapangan_id', $lapangan_id],
            ['hari', $hari],
        ])->get();

        return view('admin.harga._lastrecord', $data);
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Harga';
        $data['hari'] = hari();
        $data['harga'] = Harga::find($id);
        return view('admin.harga.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'lapangan_id' => 'required',
            'hari' => 'required',
            'harga' => 'required',
        ]);

        if ($validator->passes()) {

            $harga = Harga::find($id);

            $waktu_awal = $harga->waktu_awal;

            $batas_bawah = Harga::where([
                ['lapangan_id', $request->lapangan_id],
                ['hari', $request->hari],
                ['id', '!=', $id],
                ['waktu_akhir', '<', $request->waktu_akhir],
            ])->max('waktu_akhir');

            $batas_atas = Harga::where([
                ['lapangan_id', $request->lapangan_id],
                ['hari', $request->hari],
                ['id', '!=', $id],
                ['waktu_akhir', '>', $request->waktu_akhir],
            ])->max('waktu_awal');

            if ($request->waktu_akhir > $request->waktu_awal) {

                if ($batas_bawah > 0 && $batas_atas > 0) {
                    if ($request->waktu_awal > $batas_bawah && $request->waktu_akhir < $batas_atas) {
                        $harga->update($request->all());
                        return response()->json(['success' => 'Berhasil mengubah data Harga']);
                    } else {
                        $error = [
                            'waktu_awal' => ["Batas bawah waktu awal adalah " . futsalTime($batas_bawah) . ", waktu awal harus lebih besar dari batas bawah"],
                            'waktu_akhir' => ["Batas atas waktu akhir adalah " . futsalTime($batas_atas) . ", waktu akhir harus lebih kecil dari batas atas"],
                        ];
                        return response()->json(['error' => $error]);
                    }
                } else if ($batas_bawah > 0 && $batas_atas == 0) {
                    if ($request->waktu_awal > $batas_bawah) {
                        $harga->update($request->all());
                        return response()->json(['success' => 'Berhasil mengubah data Harga']);
                    } else {
                        $error = [
                            'waktu_awal' => ["Batas bawah waktu awal adalah " . futsalTime($batas_bawah) . ", waktu awal harus lebih besar dari batas bawah"],
                        ];
                        return response()->json(['error' => $error]);
                    }
                } else if ($batas_bawah == 0 && $batas_atas > 0) {
                    if ($request->waktu_akhir < $batas_atas) {
                        $harga->update($request->all());
                        return response()->json(['success' => 'Berhasil mengubah data Harga']);
                    } else {
                        $error = [
                            'waktu_akhir' => ["Batas atas waktu akhir adalah " . futsalTime($batas_atas) . ", waktu akhir harus lebih kecil dari batas atas"],
                        ];
                        return response()->json(['error' => $error]);
                    }
                }else{
                    $harga->update($request->all());
                    return response()->json(['success' => 'Berhasil mengubah data Harga']);
                }

            } else {
                $error = [
                    'waktu_awal' => ['Silakan cek kembali pengaturan waktu, waktu awal harus lebih besar dari waktu akhir terakhir'],
                    'waktu_akhir' => ['Silakan cek kembali pengaturan waktu, harus lebih besar dari waktu awal'],
                ];
                return response()->json(['error' => $error]);
            }

        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        $harga = Harga::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }

}
