<?php

function generateId($id, $prefix)
{
    if ($id < 10) {
        $id = '0000' . $id;
    } else if ($id >= 10 && $id < 100) {
        $id = '000' . $id;
    } else if ($id >= 100 && $id < 1000) {
        $id = '00' . $id;
    } else if ($id >= 1000 && $id <= 10000) {
        $id = '0' . $id;
    }

    return $prefix .''. $id;
}

function torp($number)
{
    if ($number >= 0) {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
    } else {
        $num = explode('-', $number);
        return 'Rp. - ' . strrev(implode('.', str_split(strrev(strval($num[1])), 3)));
    }
}

function futsalTime($time)
{
    if($time <= 9){
        $time = "0$time.00";
    }else{
        $time = "$time.00";
    }

    return $time;
}

function hari()
{
    $hari = array(
        '' => '- Pilih Hari -',
        'Senin' => 'Senin',
        'Selasa' => 'Selasa',
        'Rabu' => 'Rabu',
        'Kamis' => 'Kamis',
        'Jumat' => 'Jumat',
        'Sabtu' => 'Sabtu',
        'Minggu' => 'Minggu'
    );

    return $hari;
}

function waktu()
{
    for ($i=8; $i <= 24 ; $i++) { 
        if($i <=9){
            $waktu[$i] = "0$i.00";
        }else{
            $waktu[$i] = "$i.00";
        }
    }

    return $waktu;
}

function namaHari($date)
{
    $daftar_hari = array(
        0 => 'Minggu',
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
    );

    $index_hari = date('w', strtotime($date));
    
    return $daftar_hari[$index_hari];
}

function indoDate($date)
{
    $date = explode('-', $date);
    return "$date[2]-$date[1]-$date[0]";
}

function indoFullDateDay($datetime)
{
    $datetime = explode(' ', $datetime);
    $date = explode('-', $datetime[0]);

    if($date[1] == 1){
        $bulan = 'Januari';
    }else if($date[1] == 2){
        $bulan = 'Februari';
    }else if($date[1] == 3){
        $bulan = 'Maret';
    }else if($date[1] == 4){
        $bulan = 'April';
    }else if($date[1] == 5){
        $bulan = 'Mei';
    }else if($date[1] == 6){
        $bulan = 'Juni';
    }else if($date[1] == 7){
        $bulan = 'Juli';
    }else if($date[1] == 8){
        $bulan = 'Agustus';
    }else if($date[1] == 9){
        $bulan = 'September';
    }else if($date[1] == 10){
        $bulan = 'Oktober';
    }else if($date[1] == 11){
        $bulan = 'November';
    }else if($date[1] == 12){
        $bulan = 'Desember';
    }

    return $date[2].' '.$bulan.' '.$date[0];

}

function indoFullDate($date)
{
    $date = explode('-', $date);

    if($date[1] == 1){
        $bulan = 'Januari';
    }else if($date[1] == 2){
        $bulan = 'Februari';
    }else if($date[1] == 3){
        $bulan = 'Maret';
    }else if($date[1] == 4){
        $bulan = 'April';
    }else if($date[1] == 5){
        $bulan = 'Mei';
    }else if($date[1] == 6){
        $bulan = 'Juni';
    }else if($date[1] == 7){
        $bulan = 'Juli';
    }else if($date[1] == 8){
        $bulan = 'Agustus';
    }else if($date[1] == 9){
        $bulan = 'September';
    }else if($date[1] == 10){
        $bulan = 'Oktober';
    }else if($date[1] == 11){
        $bulan = 'November';
    }else if($date[1] == 12){
        $bulan = 'Desember';
    }

    return $date[2].' '.$bulan.' '.$date[0];

}

function to_month($m){
    if($m == 1){
        return $bulan = 'Januari';
    }else if($m == 2){
        return $bulan = 'Februari';
    }else if($m == 3){
        return $bulan = 'Maret';
    }else if($m == 4){
        return $bulan = 'April';
    }else if($m == 5){
        return $bulan = 'Mei';
    }else if($m == 6){
        return $bulan = 'Juni';
    }else if($m == 7){
        return $bulan = 'Juli';
    }else if($m == 8){
        return $bulan = 'Agustus';
    }else if($m == 9){
        return $bulan = 'September';
    }else if($m == 10){
        return $bulan = 'Oktober';
    }else if($m == 11){
        return $bulan = 'November';
    }else if($m == 12){
        return $bulan = 'Desember';
    }

}

function mysqlDate($date)
{
    $date = explode('-', $date);
    return "$date[2]-$date[1]-$date[0]";
}

function takeDate($datetime)
{
    $date = explode(' ', $datetime);
    return $date[0];
}