<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harga extends Model
{
    protected $fillable = [
        'lapangan_id', 'hari', 'waktu_awal', 'waktu_akhir', 'harga',
        'created_at', 'updated_at'
    ];

    public function lapangan()
    {
        return $this->belongsTo('App\Lapangan');
    }
}
