<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = [
        'lapangan_id', 'hari', 'waktu_awal', 'waktu_akhir', 'durasi', 'status',
        'created_at', 'updated_at'
    ];

    public function lapangan()
    {
        return $this->belongsTo('App\Lapangan');
    }

    public function booking()
    {
        return $this->hasOne('App\Booking');
    }
}
