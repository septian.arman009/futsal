<?php
date_default_timezone_set('Asia/Jakarta');
/* Route For Main View */
Route::get('/', function () {
    return redirect('/login');
});

/* Route For Auth */
Auth::routes();

/* Route For Main Menu */
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'role:admin|member'],
    ],
    function () {
        //Route Home
        Route::get('/home', 'admin\HomeController@index');
        
        //Route Change Password
        Route::get('change_password', [
            'as' => 'change_password',
            'uses' => 'Admin\UsersController@changePassword',
        ]);
        Route::post('store_password', [
            'as' => 'store_password',
            'uses' => 'Admin\UsersController@storeNewPassword',
        ]);

    }
);

Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'role:admin'],
    ],
    function () {
        //Route Resource        
        Route::resource('master/users', 'Admin\UsersController');
        Route::resource('master/lapangan', 'Admin\LapanganController');
        Route::resource('master/harga', 'Admin\HargaController');

        //Route Booking
        Route::get('transaksi/booking/create/{date}', [
            'as' => 'booking.create',
            'uses' => 'Admin\BookingController@create',
        ]);

        Route::post('transaksi/booking/form_booking', [
            'as' => 'booking.form_booking',
            'uses' => 'Admin\BookingController@formBooking',
        ]);

        Route::post('transaksi/booking_list/form_booking_edit', [
            'as' => 'booking.form_booking_edit',
            'uses' => 'Admin\BookingController@formBookingEdit',
        ]);

        Route::post('transaksi/booking/store', [
            'as' => 'booking.store',
            'uses' => 'Admin\BookingController@store',
        ]);

        Route::put('transaksi/booking_list/update/{booking_id}', [
            'as' => 'booking.update',
            'uses' => 'Admin\BookingController@update',
        ]);

        Route::get('transaksi/booking_list', [
            'as' => 'booking.index',
            'uses' => 'Admin\BookingController@index',
        ]);

        Route::get('transaksi/booking_list/{booking_id}/edit', [
            'as' => 'booking.edit',
            'uses' => 'Admin\BookingController@edit',
        ]);

        Route::delete('transaksi/booking/destroy/{booking_id}', [
            'as' => 'booking.destroy',
            'uses' => 'Admin\BookingController@destroy',
        ]);

        //Route Tagihan
        Route::get('transaksi/tagihan', [
            'as' => 'tagihan.index',
            'uses' => 'Admin\PembayaranController@index',
        ]);

        Route::get('transaksi/tagihan/{user_id}/detail', [
            'as' => 'tagihan.detail',
            'uses' => 'Admin\PembayaranController@detailTagihan',
        ]);

        Route::post('transaksi/store', [
            'as' => 'tagihan.store',
            'uses' => 'Admin\PembayaranController@store',
        ]);

        //Route Pembayaran
        Route::get('pembayaran/laporan/{tahun}', [
            'as' => 'pembayaran.laporan',
            'uses' => 'Admin\PembayaranController@laporanPembayaran',
        ]);

        Route::get('pembayaran/laporan/{user_id}/{tahun}/detail', [
            'as' => 'pembayaran.detail',
            'uses' => 'Admin\PembayaranController@detailPembayaran',
        ]);

        Route::post('pembayaran/koreksi', [
            'as' => 'pembayaran.koreksi',
            'uses' => 'Admin\PembayaranController@koreksi',
        ]);

        Route::get('pembayaran/chart/{tahun}', [
            'as' => 'pembayaran.chart',
            'uses' => 'Admin\PembayaranController@chart',
        ]);

        //Last Record Harga
        Route::get('last_record_harga/{lapangan_id}/{hari}', 'Admin\HargaController@lastRecord');

    }
);


Route::get('auth/verify/{token}', 'Auth\RegisterController@verify');
Route::get('auth/send-verification', 'Auth\RegisterController@sendVerification');
